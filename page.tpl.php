<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $language->language; ?>" xml:lang="<?php echo $language->language; ?>">

<head>
  <title><?php if (isset($head_title )) { echo $head_title; } ?></title>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <?php echo $head; ?>  
  <script type="text/javascript" src="<?php echo get_full_path_to_theme(); ?>/script.js"></script>  
  <?php echo $styles ?>
  <?php echo $scripts ?>
  <!--[if IE 6]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie6.css" type="text/css" /><![endif]-->  
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
    <div class="PageBackgroundGradient"></div>
<div class="Main">
<div class="Sheet">
    <div class="Sheet-tl"></div>
    <div class="Sheet-tr"><div></div></div>
    <div class="Sheet-bl"><div></div></div>
    <div class="Sheet-br"><div></div></div>
    <div class="Sheet-tc"><div></div></div>
    <div class="Sheet-bc"><div></div></div>
    <div class="Sheet-cl"><div></div></div>
    <div class="Sheet-cr"><div></div></div>
    <div class="Sheet-cc"></div>
    <div class="Sheet-body">
<div class="Header">
    <div class="Header-png"></div>
    <div class="Header-jpeg"></div>
<div class="logo">
 <?php if ($site_name) : ?>
 <h1 class="logo-name"><a href="<?php echo check_url($base_path); ?>" title = "<?php echo $site_name; ?>"><?php echo $site_name; ?></a></h1>
 <?php endif; ?>
 <?php if ($site_slogan) : ?>
 <div class="logo-text"><?php echo $site_slogan; ?></div>
 <?php endif; ?>
</div>

</div>
<div class="nav">
<?php
  $menu_name = variable_get('menu_default_node_menu', 'primary-links');
  $tree = menu_tree_all_data($menu_name);
  echo art_menu_tree_output_d6($tree);
?>
<div class="l"></div>
<div class="r"><div></div></div>
</div>
<div class="contentLayout">
<div class="content">
<div class="Post">
    <div class="Post-body">
<div class="Post-inner">
<div class="PostContent">
<?php if ($breadcrumb): print theme('breadcrumb', $breadcrumb); endif; ?>
<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
<?php if ($title): print '<h2 class="PostHeaderIcon-wrapper'. ($tabs ? ' with-tabs' : '') .'">'. $title .'</h2>'; endif; ?>
<?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
<?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
<?php if ($mission): print '<div id="mission">' . $mission . '</div>'; endif; ?>
<?php if ($help): print $help; endif; ?>
<?php if ($show_messages && $messages): print $messages; endif; ?>
<?php print art_content_replace($content); ?>

</div>
<div class="cleared"></div>

</div>

    </div>
</div>

</div>
<div class="sidebar2">
<?php
if (isset($sidebar_right)) print $sidebar_right;
else if (isset($right)) print $right; 
?>
</div>

</div>
<div class="cleared"></div>
<div class="Footer">
    <div class="Footer-inner">
        <a href="<?php $feedsUrls = array_keys(drupal_add_feed()); if(isset($feedsUrls[0]) && strlen($feedsUrls[0])>0) {echo $feedsUrls[0];} ?>" class="rss-tag-icon" title="RSS"></a>
        <div class="Footer-text">
        <?php echo $footer; ?>
        </div>
    </div>
    <div class="Footer-background"></div>
</div>

    </div>
</div>
<p class="page-footer">
  <?php echo $footer_message; ?>

</p>

</div>


<?php if ($closure_region): ?>
  <div id="closure-blocks"><?php print $closure_region; ?></div>
<?php endif; ?>
<?php print $closure; ?>

</body>
</html>